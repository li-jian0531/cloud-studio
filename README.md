本项目来源于[《腾讯云 Cloud Studio 实战训练营》](https://marketing.csdn.net/p/06a21ca7f4a1843512fa8f8c40a16635)的参赛作品，该作品在腾讯云 [Cloud Studio](https://www.cloudstudio.net/?utm=csdn) 中运行无误。


## 程序介绍
本次程序代码是通过GPT工具 + [CLoud Studio](htps://www.cloudstudio.net/) 辅助编程的方式，完成了这个通过Python自动化处理ExceL工资核算的案例，

## 代码启动
`python demo.py`

该命令会运行主程序，然后将考勤扣款、个税、实发工资几项按照规算出来

并输出一个名为 salary_output.xlsx 的最终文件
