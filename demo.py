import pandas as pd

# 读取excel文件
df = pd.read_excel('salary.xlsx',sheet_name='Sheet1')

# 计算考勤扣除金额
late_counts = df['迟到次数']

df['考勤扣除金额'] =  late_counts.apply(lambda x: max((x-3)*100 ,0))

#计算个税扣除
taxable_income = df['工资基数'] - df['五险一金扣除'] - df['考勤扣除金额']
df['个税扣除']= taxable_income.apply(lambda x:
min(x*0.03,90) if x <=3000 else
min(x*0.1,210) if 3000<x<=12000 else
min(x*0.2,1410) if 12000<x<=25000 else
min(x*0.25,2660) if 25000<x<=35000 else
min(x*0.3,4410) if 35000<X<=55000 else
min(x*0.35,7160) if 55000<x<-80000 else
X *0.45)
#计算实发工资
df['实发工资']= df['工资基数'] - df['五险一金扣除'] - df['考勤扣除金额'] - df['个税扣除']


# 打印整体数据
print(df)

# 将数据写回到excel文件

writer = pd.ExcelWriter('salary_output.xlsx')
df.to_excel(writer, 'Sheet1', index=False) 
writer.close()

print('结果已写入Excel!')